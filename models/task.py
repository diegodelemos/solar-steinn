from google.appengine.ext import ndb


class Task(ndb.Model):
    """
    Model that represents a task

    """
    ## @var title
    # Task title
    ## @var like_rating
    # Integer that represents on a scale from 0 to 5 how much the user like this task.
    ## @var difficulty_rating
    # Integer that represents on a scale from 0 to 5 how the user
    ## @var deadline
    # Datetime when the task is going to end
    ## @var description
    # Task description
    title = ndb.StringProperty()
    like_rating = ndb.IntegerProperty()
    difficulty_rating = ndb.IntegerProperty()
    deadline = ndb.DateTimeProperty()
    description = ndb.TextProperty()

@ndb.transactional
@ndb.toplevel
def new_task(platform_user, title, deadline, like_rating,
             difficulty_rating, description):
    """
    Creates a new task
    @param platform_user Task owner and creator
    @param title task title
    @param deadline task deadline
    @param like_rating Task like rating
    @param difficulty_rating Task difficulty rating
    @param description Task description
    """
    new_task = Task(title=title, like_rating=like_rating,
                    difficulty_rating=difficulty_rating,
                    deadline=deadline, description=description)

    task_key = new_task.put()
    platform_user.task_list.append(task_key)
    platform_user.put()

@ndb.transactional
@ndb.toplevel
def update_task(urlsafe_key, title, deadline, like_rating,
                difficulty_rating, description):
    """
    Updates a task properties
    @param urlsafe_key URL-safe string that represents the task key
    @param title Task title to be updated
    @param deadline Task deadline to be updated
    @param like_rating Task like rating to be updated
    @param difficulty_rating  Task difficulty rating to be updated
    @param description Task description to be updated
    """
    current_task = get_task_from_urlsafe(urlsafe_key)
    current_task.deadline = deadline
    current_task.title = title
    current_task.description = description
    current_task.like_rating = like_rating
    current_task.difficulty_rating = difficulty_rating

    current_task.put()


def get_task_from_urlsafe(urlsafe_key):
    """
    Converts a urlsafe key into a task object
    @param urlsafe_key: urlsafe key
    @return Task object
    """
    task_key = ndb.Key(urlsafe=urlsafe_key)
    return task_key.get()


@ndb.transactional
@ndb.toplevel
def delete_task(platform_user, urlsafe_task_key):
    """
    Deletes a task
    @param platform_user task owner
    @param urlsafe_task_key task urlsafe key
    """
    task_key = ndb.Key(urlsafe=urlsafe_task_key)
    platform_user.task_list.remove(task_key)
    platform_user.put()
    task_key.delete()