from google.appengine.ext import ndb

class PlatformUser(ndb.Model):
    """
    Model that represents a User for this platform.

    """
    ## @var id
    # user's Google id
    ## @var mood_rating
    # integer that represents the user's mood
    ## @var task_list
    # user's task list
    ## @var utc_offset
    # integer that represents the difference between the UTC time and the user's time
    id = ndb.StringProperty()
    mood_rating = ndb.IntegerProperty()
    task_list = ndb.KeyProperty(kind="Task", repeated=True)
    utc_offset = ndb.IntegerProperty()

@ndb.transactional
@ndb.toplevel
def update_user_mood(p_user, mood):
    """
    Updates the user's mood
    @param p_user user to be updated
    @param mood mood rating number
    @return
    """
    p_user.mood_rating = mood
    p_user.put()


@ndb.transactional
@ndb.toplevel
def update_user_utc_offset(p_user, offset):
    """
    Updates the user's utc_offset
    @param p_user user to be updated
    @param offset UTC user's offset.
    """
    p_user.utc_offset = offset
    p_user.put()
