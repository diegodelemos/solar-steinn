
from handlers.base import AppHandler
from google.appengine.ext import ndb
from models import platform_user

class Prefferences(AppHandler):
    """
    Switch users mood_rating and redirects back to the task list, also updates the user time zone offset.
    """
    def __init__(self, request=None, response=None):
        super(Prefferences, self).__init__(request, response)
        self.user_check()

    def post(self):
        try:
            p_user = self.platform_user()
            mood = int(self.request.get('mood'))
            utc_offset = int(self.request.get('utc-offset'))
            if mood > 5 or mood < 1:
                raise ValueError("Incorrect mood rating number")
            else:
                platform_user.update_user_mood(p_user, mood)
                platform_user.update_user_utc_offset(p_user, utc_offset)

            self.redirect("/list")
        except ValueError as ve:
            self.send_alert(msg_type="warning", msg=ve.message, url="/list", url_text="my list")
        except:
            self.send_alert(msg_type="danger")