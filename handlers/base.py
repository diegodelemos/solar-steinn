__author__ = 'Diego'


import jinja2
import webapp2
from google.appengine.api import users
from main import template_dir
from models import platform_user

# Initialize the jinja2 environment
jinja_environment = jinja2.Environment(autoescape=True,
                                       loader=jinja2.FileSystemLoader(template_dir),
                                       extensions=['jinja2.ext.autoescape'])


class AppHandler(webapp2.RequestHandler):
    """Base handler, encapsulating jinja2 functions and user api."""
    def __init__(self, request=None, response=None):
        """Initialize the handler."""
        super(AppHandler, self).__init__(request, response)
        self.section = ""
        self.user = users.get_current_user()
        self.values = {'section': self.section, 'user': self.user}

        self.jinja = jinja_environment

    def write(self, string):
        """Write an arbitrary string to the response stream."""
        self.response.out.write(string)

    def render_str(self, template_name, values=None, **kwargs):
        """Render a jinja2 template and return it as a string."""
        template = self.jinja.get_template(template_name)
        return template.render(self.values or kwargs)

    def render(self, template_name, values=None, **kwargs):
        """Render a jinja2 template using a dictionary or keyword arguments."""
        self.write(self.render_str(template_name, values or kwargs))

    def redirect_to(self, name, *args, **kwargs):
        """Redirect to a URI that corresponds to a route name."""
        self.redirect(self.uri_for(name, *args, **kwargs))

    def get_logout_url(self, redirect_uri):
        """
        Returns a logout url that redirects to redirect_uri.
        @param redirect_uri uri to redirect after login out
        @return logout url string
        """
        return users.create_logout_url(redirect_uri)

    def get_login_url(self, redirect_uri):
        return users.create_login_url(redirect_uri)

    # check if user is loged in
    def user_check(self):
        """Checks whether self.user is allowed to browse the platform"""
        if not self.user:
            self.redirect('/', abort=True)

    def platform_user(self):
        """
        Function that gets the PlatformUser entity for the actual Google app engine user. If this user
        has no PlatformUser the function creates it.
        @return Platform user entity
        """
        if self.user:
            try:
                p_user = platform_user.PlatformUser.query(platform_user.PlatformUser.id == self.user.user_id()).fetch(1)
                if p_user:
                    return p_user[0]
                else:
                    return platform_user.PlatformUser(id=self.user.user_id())
            except Exception as e:
                e.message = "error al recuperar o crear platform user"

    def add_values(self, values):
        self.values.update(values)

    def send_alert(self, msg_type="warning", msg="An error has occurred", url="/list", url_text="my list"):
        """
        Function to send messages to the user.

        @param msg_type type of the alert message. It can take four values, success, info, warning and danger.
        @param msg String that represents the text to be shown to the user inside the alert.
        @param url String that represents the url link to insert inside the alert.
        @param url_text String that represents the url in a user friendly way.
        @return redirects the user to msg.html with this parameter values.
        """
        values = {
            'type': msg_type,
            'url': url,
            'msg': msg+". Back to ",
            'url_text': url_text
        }
        self.add_values(values)
        self.render('msg.html')