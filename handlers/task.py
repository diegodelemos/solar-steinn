__author__ = 'Diego'

from datetime import datetime
from datetime import timedelta

from handlers.base import AppHandler
from libs import recommend_lib, common_functions
from models import task


class TaskList(AppHandler):
    """
    Main application view. List the current user's list of task.
    recommend_lib is used here to implement the task ordering logic.
    """
    def __init__(self, request, response):
        super(TaskList, self).__init__(request, response)
        self.user_check()

    def get(self):
        try:
            self.values['section'] = "Priority list"
            tasks = list()
            out_of_date = list()
            ordered_tasks = list()
            task_keys = self.platform_user().task_list
            user_now = common_functions.get_user_now(self.platform_user())
            if len(task_keys) > 0:
                for task_key in task_keys:
                    # checking whether the task is in time or not
                    current_task = task_key.get()
                    if current_task:
                        if current_task and current_task.deadline > user_now:
                            tasks.append(current_task)
                        else:
                            out_of_date.append(current_task)

                if len(tasks) > 1:
                    # if list has more than 1 member it means that it has to be ordered according to the user mood.
                    ordered_tasks = recommend_lib.weighted_task_list(self.platform_user(), tasks)
                else:
                    ordered_tasks = tasks

            values = {
                'tasks': ordered_tasks,
                'out_of_date': out_of_date,
                'p_user': self.platform_user(),
                'now': user_now,
                'logout_url': self.get_logout_url('/')
            }

            self.add_values(values)
            self.render('task_list.html')
        except Exception as e:
            self.send_alert(msg_type="danger", msg="An error has occurred retrieving your task list"+e.message)


###### TASK CRUD
class NewTask(AppHandler):
    """
    Both, form view and form handling are managed by NewTask handler. (Create and Update)
    """
    def __init__(self, request, response):
        super(NewTask, self).__init__(request, response)
        self.user_check()

    def get(self):
        try:
            self.values['section'] = "New task"
            values = {
                'logout_url': self.get_logout_url('/')
            }
            self.add_values(values)
            self.render('form_task.html')
        except:
            self.send_alert(msg_type="danger", msg="An error has occurred rendering the task form")

    def post(self):
        try:
            title = self.request.get('title')
            description = self.request.get('description')
            like_rating = int(self.request.get('like'))
            difficulty_rating = int(self.request.get('difficulty'))
            deadline = common_functions.convert_to_datetime(self.request.get('date'), self.request.get('time'))

            if len(title) == 0:
                raise ValueError("Title can not be empty")

            if like_rating > 5 or like_rating < 0:
                raise ValueError("Incorrect like rating number")

            if difficulty_rating > 5 or difficulty_rating < 0:
                raise ValueError("Incorrect difficulty rating number")

            if deadline < datetime.now():
                raise ValueError("Deadline must be greater than the actual date")

            # If a task is provided by the form, it is updated, otherwise it means that a new task
            # has to be created.
            if self.request.get('task_key'):
                task.update_task(self.request.get('task_key'), title, deadline,
                                 like_rating, difficulty_rating, description)
            else:
                task.new_task(self.platform_user(), title, deadline, like_rating, difficulty_rating, description)

            self.redirect("/list")
        except ValueError as ve:
            self.send_alert(msg_type="warning", msg=ve.message, url=self.request.uri, url_text="form")
        except:
            self.send_alert(msg_type="danger", msg="An error has occurred adding the task")



class EditTask(AppHandler):
    """
    Task form renderer (Read operation)
    """
    def __init__(self, request=None, response=None):
        super(EditTask, self).__init__(request, response)
        self.user_check()

    def get(self, **kwargs):
        try:
            self.values['section'] = "Edit Task"
            current_task = task.get_task_from_urlsafe(kwargs.pop('task_key'))
            values = {
                'user': self.user,
                'task': current_task,
                'logout_url': self.get_logout_url('/')
            }
            self.add_values(values)
            self.render('form_task.html')
        except:
            self.send_alert(msg_type="danger", msg="An error has occurred retrieving the task")


class DeleteTask(AppHandler):
    """
    Delete a task by it's key. (Delete)
    """
    def __init__(self, request=None, response=None):
        super(DeleteTask, self).__init__(request, response)
        self.user_check()

    def get(self, **kwargs):
        try:
            task_key_url = kwargs.pop('task_key')
            task.delete_task(self.platform_user(), task_key_url)
            self.redirect('/list')
        except:
            self.send_alert(msg_type="warning", msg="An error has occurred deleting the task")