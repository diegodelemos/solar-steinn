from handlers.base import AppHandler


class PresentationHandler(AppHandler):
    """
    First application page. Contains a description and login buttons.
    """
    def __init__(self, request=None, response=None):
        super(PresentationHandler, self).__init__(request, response)

    def get(self):
        if not self.user:
            self.values['section'] = "Presentation"
            presentation_title = "Welcome to Solarsteinn"
            presentation_content = "According to the legend, a sunstone was used by the vikings to locate the sun in a" \
                                   "completely overcast sky."
            values = {
                'presentation_title': presentation_title,
                'presentation_content': presentation_content,
                'login_url': self.get_login_url('/list'),
                'user': self.user
            }
            self.add_values(values)
            self.render('presentation.html')
        else:
            self.redirect('/list')
