__author__ = 'Diego'

import os
import jinja2
import webapp2
from google.appengine.ext import ndb



## \mainpage Main page
# \author Diego Rodríguez Rodríguez
# \version 1-04
# \details This project consists of a task list ordered by the user's mood.
# \htmlonly
# <a href="http://www.solar-steinn.appspot.com"><img src="../preview.PNG"/></a>
# \endhtmlonly


# template DIR is placed in /templates
root_dir = os.path.dirname(__file__)
template_dir = os.path.join(root_dir, 'templates')

jinja_environment = jinja2.Environment(autoescape=True,
                                       loader=jinja2.FileSystemLoader(template_dir),
                                       extensions=['jinja2.ext.autoescape'])


app = ndb.toplevel(webapp2.WSGIApplication([
    webapp2.Route('/', handler='handlers.presentation.PresentationHandler'),
    webapp2.Route('/list', handler='handlers.task.TaskList'),
    webapp2.Route('/prefferences', handler='handlers.prefferences.Prefferences'),
    webapp2.Route('/new-task', handler='handlers.task.NewTask'),
    webapp2.Route('/edit/<task_key>', handler='handlers.task.EditTask'),
    webapp2.Route('/delete/<task_key>', handler='handlers.task.DeleteTask')
], debug=True))