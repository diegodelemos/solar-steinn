var searchData=
[
  ['platform_5fuser',['platform_user',['../classhandlers_1_1base_1_1_app_handler.html#a166b92a86436bc9c1e60cf74766a7487',1,'handlers::base::AppHandler']]],
  ['platform_5fuser_2epy',['platform_user.py',['../platform__user_8py.html',1,'']]],
  ['platformuser',['PlatformUser',['../classmodels_1_1platform__user_1_1_platform_user.html',1,'models::platform_user']]],
  ['post',['post',['../classhandlers_1_1prefferences_1_1_prefferences.html#a972669b1d1b627ba0b2b876e1cfa13cb',1,'handlers.prefferences.Prefferences.post()'],['../classhandlers_1_1task_1_1_new_task.html#a4b6b901bdcc80341372cf8d4413da3c4',1,'handlers.task.NewTask.post()']]],
  ['prefferences',['Prefferences',['../classhandlers_1_1prefferences_1_1_prefferences.html',1,'handlers::prefferences']]],
  ['prefferences_2epy',['prefferences.py',['../prefferences_8py.html',1,'']]],
  ['presentation_2epy',['presentation.py',['../presentation_8py.html',1,'']]],
  ['presentationhandler',['PresentationHandler',['../classhandlers_1_1presentation_1_1_presentation_handler.html',1,'handlers::presentation']]]
];
