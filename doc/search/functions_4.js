var searchData=
[
  ['get',['get',['../classhandlers_1_1presentation_1_1_presentation_handler.html#a33b8f7c9ea6892e59386e3796919a69b',1,'handlers.presentation.PresentationHandler.get()'],['../classhandlers_1_1task_1_1_task_list.html#aa586f49b23fb6b8fcfdb48fbbdd30cc3',1,'handlers.task.TaskList.get()'],['../classhandlers_1_1task_1_1_new_task.html#ae12a52972de4d11c0f33eccdacc312fa',1,'handlers.task.NewTask.get()'],['../classhandlers_1_1task_1_1_edit_task.html#a81401d531d531129750e13452039e4b9',1,'handlers.task.EditTask.get()'],['../classhandlers_1_1task_1_1_delete_task.html#a74b773331beb2511e0e581e950669ee0',1,'handlers.task.DeleteTask.get()']]],
  ['get_5flogin_5furl',['get_login_url',['../classhandlers_1_1base_1_1_app_handler.html#a677d4ea8792ded6d1b4af12eb18604f9',1,'handlers::base::AppHandler']]],
  ['get_5flogout_5furl',['get_logout_url',['../classhandlers_1_1base_1_1_app_handler.html#a37a292a2753ada901edccdee31995f62',1,'handlers::base::AppHandler']]],
  ['get_5flonger_5fdeadline',['get_longer_deadline',['../namespacelibs_1_1recommend__lib.html#ab219993c4a15ac026c4ce1960bb2d716',1,'libs::recommend_lib']]],
  ['get_5ftask_5ffrom_5furlsafe',['get_task_from_urlsafe',['../namespacemodels_1_1task.html#a035aeef51d30a162f37e068ce63a6229',1,'models::task']]],
  ['get_5fuser_5fnow',['get_user_now',['../namespacelibs_1_1common__functions.html#af3af17dcf3ba33772c4e3d06378542ec',1,'libs::common_functions']]],
  ['get_5fweight',['get_weight',['../namespacelibs_1_1recommend__lib.html#ad6f729eae4a941609c4110ec3c5bd728',1,'libs::recommend_lib']]],
  ['get_5fweights_5fby_5fmood',['get_weights_by_mood',['../namespacelibs_1_1recommend__lib.html#a1c3c6690c38789b52ff1d6a64009cd61',1,'libs::recommend_lib']]]
];
