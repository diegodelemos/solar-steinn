var hierarchy =
[
    [ "Model", null, [
      [ "models.platform_user.PlatformUser", "classmodels_1_1platform__user_1_1_platform_user.html", null ],
      [ "models.task.Task", "classmodels_1_1task_1_1_task.html", null ]
    ] ],
    [ "RequestHandler", null, [
      [ "handlers.base.AppHandler", "classhandlers_1_1base_1_1_app_handler.html", [
        [ "handlers.prefferences.Prefferences", "classhandlers_1_1prefferences_1_1_prefferences.html", null ],
        [ "handlers.presentation.PresentationHandler", "classhandlers_1_1presentation_1_1_presentation_handler.html", null ],
        [ "handlers.task.DeleteTask", "classhandlers_1_1task_1_1_delete_task.html", null ],
        [ "handlers.task.EditTask", "classhandlers_1_1task_1_1_edit_task.html", null ],
        [ "handlers.task.NewTask", "classhandlers_1_1task_1_1_new_task.html", null ],
        [ "handlers.task.TaskList", "classhandlers_1_1task_1_1_task_list.html", null ]
      ] ]
    ] ]
];