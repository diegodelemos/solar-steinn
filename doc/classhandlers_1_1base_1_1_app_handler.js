var classhandlers_1_1base_1_1_app_handler =
[
    [ "__init__", "classhandlers_1_1base_1_1_app_handler.html#ad1ac096f21e52684c05b2a1c0ad97e43", null ],
    [ "add_values", "classhandlers_1_1base_1_1_app_handler.html#a3d8e82098271fad61389faab9726b600", null ],
    [ "get_login_url", "classhandlers_1_1base_1_1_app_handler.html#a677d4ea8792ded6d1b4af12eb18604f9", null ],
    [ "get_logout_url", "classhandlers_1_1base_1_1_app_handler.html#a37a292a2753ada901edccdee31995f62", null ],
    [ "platform_user", "classhandlers_1_1base_1_1_app_handler.html#a166b92a86436bc9c1e60cf74766a7487", null ],
    [ "redirect_to", "classhandlers_1_1base_1_1_app_handler.html#a90dc0aed8a97e9f6316175876a820393", null ],
    [ "render", "classhandlers_1_1base_1_1_app_handler.html#aa7fb4115e243088de1008b19ab68d305", null ],
    [ "render_str", "classhandlers_1_1base_1_1_app_handler.html#ac11a44a59229f388d2731f7bf1d1f3f1", null ],
    [ "send_alert", "classhandlers_1_1base_1_1_app_handler.html#a5f021edb4ce0f950cd494f8c73f080f2", null ],
    [ "user_check", "classhandlers_1_1base_1_1_app_handler.html#a372fecb26c9b5723015d50d878c90c33", null ],
    [ "write", "classhandlers_1_1base_1_1_app_handler.html#a6b484c4299a106e6ccf894f313a593b8", null ],
    [ "jinja", "classhandlers_1_1base_1_1_app_handler.html#a9ed4fae5b8e7d01b6bb682dac8acea54", null ],
    [ "section", "classhandlers_1_1base_1_1_app_handler.html#acdc5646c9a9ed4c0c6a7f7583e1f0aa5", null ],
    [ "user", "classhandlers_1_1base_1_1_app_handler.html#a30ea5fa8528364fa44608c7e532614a7", null ],
    [ "values", "classhandlers_1_1base_1_1_app_handler.html#a5bf53d4ee73a10aad60b7388d4823900", null ]
];