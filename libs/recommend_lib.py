import datetime
from libs import common_functions

def weighted_task_list(p_user, tasks):
    """
    Sorts a list of task by deadline, like and difficulty parameters.
    @return list of ordered tasks by importance for the user
    """
    ordered_tasks = []
    if len(tasks) > 1:
        now = common_functions.get_user_now(p_user)
        MAX_DIFFICULTY = 5
        MAX_LIKE = 5
        MAX_DEADLINE = get_longer_deadline(now, tasks)

        deadline_max_weight = 70
        weights = get_weights_by_mood(p_user, 30)
        like_max_weight = weights.get('like')
        difficulty_max_weight = weights.get('difficulty')

        # weighted_task_list : [[TASK1, TASK1_GLOBAL_WEIGHT], ..., [TASKN, TASKN_GLOBAL_WEIGHT]]
        weighted_task_list = list()

        for task in tasks:
            ## time left to deadline
            time = int((task.deadline - now).days)
            # deadline parameter weight
            task_deadline_weight = get_weight(time, deadline_max_weight, MAX_DEADLINE, inverse_relation=True)
            # like parameter weight
            task_like_weight = get_weight(task.like_rating, like_max_weight, MAX_LIKE)
            # difficulty parameter weight
            task_difficulty_weight = get_weight(task.difficulty_rating, difficulty_max_weight, MAX_DIFFICULTY)

            weighted_task_list.append([task, (task_deadline_weight + task_like_weight + task_difficulty_weight)])

        # order weighted task list by weight.
        sorted_weighted_tasks = sorted(weighted_task_list, key=lambda list: list[1], reverse=True)

        # return only task entities
        for task in sorted_weighted_tasks:
            ordered_tasks.append(task[0])

    else:
        ordered_tasks = tasks

    return ordered_tasks


def get_weight(parameter, percentage, max_parameter, inverse_relation=False):
    """
    Function to calculate parameter global importance. By default the relation between the parameter
    and the importance is a direct relation, but it can be switched to inverse_relation.
    @param parameter: Represents the value of the parameter. i.e. like_rating = 4 (out of max_parameter)
    @param percentage: Represents the global percentage that parameter matters.
    @param max_parameter: Maximum parameter value.
    @param inverse_relation: Boolean that represents whether the relation between parameter and its importance
                             is direct or inverse.
    @return: Value between 0 and percentage representing the parameter's global importance.
    """
    importance = None
    if not inverse_relation:
        importance = (parameter * percentage) / max_parameter
    else:
        if max_parameter > 0:
            importance = (percentage*(max_parameter-parameter)) / max_parameter
        else:
            ## if the closest deadline is in 0 days, all have an importance of Percentage(full) in the time parameter
            importance = percentage

    return importance

def get_longer_deadline(now, tasks):
    """
    Function that calculates the time left to reach the latest deadline task in tasks list.
    @param tasks: list of task entities
    @return a number that represents the time left to the latest deadline task.
    """
    max_time = 0
    for task in tasks:
        actual_task_time = int((task.deadline - now).days)
        if actual_task_time > max_time:
            max_time = actual_task_time

    return max_time


def get_weights_by_mood(p_user, MAX_WEIGHT, MAX_MOOD_RATING=5):
    """
    Calculates the like and difficult global weights by the user's mood.
    The strategy to calculate it is to calculate the difficulty weight and substrate this
    weight to the max_like_and_difficult_rate = 50
    @return Dictionary composed by the like and difficult weights.
    """
    LIKE_MAX_WEIGHT = 0
    DIFFICULTY_MAX_WEIGHT = 0
    if p_user.mood_rating:
        DIFFICULTY_MAX_WEIGHT = (p_user.mood_rating * MAX_WEIGHT) / MAX_MOOD_RATING
        LIKE_MAX_WEIGHT = MAX_WEIGHT - DIFFICULTY_MAX_WEIGHT
    else:
        ## DEFAULT VALUES
        LIKE_MAX_WEIGHT = 20
        DIFFICULTY_MAX_WEIGHT = 30

    return {'like': LIKE_MAX_WEIGHT, 'difficulty': DIFFICULTY_MAX_WEIGHT}