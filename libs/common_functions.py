from datetime import datetime
from datetime import timedelta


def convert_to_datetime(date_string, time_string):
    """
    Converts a date string with '-' separator and time string with ':' separator and to python datetime format.
    @param date_string: date string that comes from client
    @param time_string: time string that comes from client
    @return Date in python datetime format
    """
    try:
        date_list = date_string.split("-")
        time_list = time_string.split(":")
        toret = datetime(int(date_list[0]), int(date_list[1]), int(date_list[2]),
                         int(time_list[0]), int(time_list[1]), 0, 0)

        return toret
    except:
        raise TypeError


def get_user_now(p_user):
    """
    Calculates the user time using PlatformUser utc_offset which is the difference with UTC.
    @param p_user PlatformUser instance
    @return The user's time, depending on his time zone.
    """
    offset = p_user.utc_offset
    if p_user.utc_offset:
        if offset < 0:
            offset = abs(offset)
            user_now_time = datetime.now() + timedelta(minutes=offset)
        else:
            user_now_time = datetime.now() - timedelta(minutes=offset)
    else:
        user_now_time = datetime.now()

    return user_now_time